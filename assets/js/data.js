var elements = {};

elements.body = [{
        'stroke': ' ',
        'fill': 'E8BF85',
        'gender': 'F',
        'data': 'mujer/body.svg'
    },
    {
        'stroke': ' ',
        'fill': 'E8BF85',
        'gender': 'M',
        'data': 'hombre/body.svg'
    }
];
elements.pantalon = [{
        'name': 'Leggins',
        'gender': 'F',
        'data': {
            'stroke': '',
            'fill': '515C82',
            'elm': 'mujer/M_pantalon1.svg'
        }
    },
    {
        'name': 'Mezclilla',
        'gender': 'F',
        'data': {
            'stroke': '',
            'fill': '000080',
            'elm': 'mujer/M_pantalon2.svg'
        }
    },
    {
        'name': 'Mezclilla',
        'gender': 'M',
        'data': {
            'stroke': '',
            'fill': '000080',
            'elm': 'hombre/H_pantalon1.svg'
        }
    },
    {
        'name': 'Mezclilla',
        'gender': 'M',
        'data': {
            'stroke': '',
            'fill': '000080',
            'elm': 'hombre/H_pantalon2.svg'
        }
    }
];
elements.camisa = [{
        'name': 'Sueter',
        'gender': 'F',
        'data': {
            'stroke': '',
            'fill': '6E8D81',
            'elm': 'mujer/M_playera1.svg'
        }
    },
    {
        'name': 'Sueter 2',
        'gender': 'F',
        'data': {
            'stroke': '',
            'fill': '6E8D81',
            'elm': 'mujer/M_playera2.svg'
        }
    },
    {
        'name': 'Sueter 2',
        'gender': 'M',
        'data': {
            'stroke': '',
            'fill': '6E8D81',
            'elm': 'hombre/H_playera1.svg'
        }
    },
    {
        'name': 'Sueter 2',
        'gender': 'M',
        'data': {
            'stroke': '',
            'fill': '6E8D81',
            'elm': 'hombre/H_playera2.svg'
        }
    }
];
elements.zapatos = [{
        'name': 'Flats',
        'gender': 'F',
        'data': {
            'stroke': '',
            'fill': '333333',
            'elm': 'mujer/M_zapatos1.svg'
        }
    },
    {
        'name': 'Flats',
        'gender': 'F',
        'data': {
            'stroke': '',
            'fill': '333333',
            'elm': 'mujer/M_zapatos2.svg'
        }
    },
    {
        'name': 'Flats',
        'gender': 'M',
        'data': {
            'stroke': '',
            'fill': '333333',
            'elm': 'hombre/H_zapato1.svg'
        }
    },
    {
        'name': 'Flats',
        'gender': 'M',
        'data': {
            'stroke': '',
            'fill': '333333',
            'elm': 'hombre/H_zapato2.svg'
        }
    }
];
elements.cabello = [{
        'name': 'PeliRojo',
        'gender': 'F',
        'data': {
            'stroke': '',
            'fill': 'a70000',
            'elm': 'mujer/M_cabello1.svg'
        }
    },
    {
        'name': 'PeliRojo',
        'gender': 'F',
        'data': {
            'stroke': '',
            'fill': 'a70000',
            'elm': 'mujer/M_cabello2.svg'
        }
    },
    {
        'name': 'PeliRojo',
        'gender': 'M',
        'data': {
            'stroke': '',
            'fill': 'a70000',
            'elm': 'hombre/H_cabello1.svg'
        }
    },
    {
        'name': 'PeliRojo',
        'gender': 'M',
        'data': {
            'stroke': '',
            'fill': 'a70000',
            'elm': 'hombre/H_cabello2.svg'
        }
    },
];
elements.rostro = [{
        'name': 'Tipo A',
        'gender': 'F',
        'data': {
            'stroke': '',
            'fill': '111',
            'elm': 'mujer/M_cara1.svg'
        }
    },
    {
        'name': 'Tipo A',
        'gender': 'F',
        'data': {
            'stroke': '',
            'fill': '111',
            'elm': 'mujer/M_cara2.svg'
        }
    },
    {
        'name': 'Tipo A',
        'gender': 'M',
        'data': {
            'stroke': '',
            'fill': '111',
            'elm': 'hombre/H_cara1.svg'
        }
    },
    {
        'name': 'Tipo A',
        'gender': 'M',
        'data': {
            'stroke': '',
            'fill': '111',
            'elm': 'hombre/H_cara2.svg'
        }
    }
];
elements.gorro = [{
        'name': 'Ganster',
        'gender': 'F',
        'data': {
            'stroke': '',
            'fill': '475661',
            'elm': 'mujer/M_gorro1.svg'
        }
    },
    {
        'name': 'Ganster',
        'gender': 'F',
        'data': {
            'stroke': '',
            'fill': '475661',
            'elm': 'mujer/M_gorro2.svg'
        }
    },
    {
        'name': 'Ganster',
        'gender': 'M',
        'data': {
            'stroke': '',
            'fill': '475661',
            'elm': 'hombre/H_gorro1.svg'
        }
    },
    {
        'name': 'Ganster',
        'gender': 'M',
        'data': {
            'stroke': '',
            'fill': '475661',
            'elm': 'hombre/H_gorro2.svg'
        }
    }
];
elements.lentes = [{
        'name': 'Rock',
        'gender': 'F',
        'data': {
            'stroke': '',
            'fill': '000000',
            'elm': 'mujer/M_lentes1.svg'
        }
    },
    {
        'name': 'Fashion',
        'gender': 'F',
        'data': {
            'stroke': '',
            'fill': '43414D',
            'elm': 'mujer/M_lentes2.svg'
        }
    },
    {
        'name': 'Fashion',
        'gender': 'M',
        'data': {
            'stroke': '',
            'fill': '43414D',
            'elm': 'hombre/H_lentes1.svg'
        }
    },
    {
        'name': 'Fashion',
        'gender': 'M',
        'data': {
            'stroke': '',
            'fill': '43414D',
            'elm': 'hombre/H_lentes2.svg'
        }
    }
];
elements.cuello_accesorio = [{
        'name': 'Collar',
        'gender': 'F',
        'data': {
            'stroke': '',
            'fill': 'ffffff',
            'elm': 'mujer/M_accesorio1.svg'
        }
    },
    {
        'name': 'Collar',
        'gender': 'F',
        'data': {
            'stroke': '',
            'fill': 'ffffff',
            'elm': 'mujer/M_accesorio2.svg'
        }
    },
    {
        'name': 'Collar',
        'gender': 'M',
        'data': {
            'stroke': '',
            'fill': 'ffffff',
            'elm': 'hombre/H_accesorio1.svg'
        }
    },
    {
        'name': 'Collar',
        'gender': 'M',
        'data': {
            'stroke': '',
            'fill': 'ffffff',
            'elm': 'hombre/H_accesorio2.svg'
        }
    }
];