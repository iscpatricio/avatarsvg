
var con_draw_list = 0;
var accordion = "";
var gender = "F";
var userId = "";
var elements_ = {};
var radio;
$(function () {
    var _c = 0;
    $("#group_gender label").click(function (e) {
        e.stopImmediatePropagation();
        //e.stopPropagation();
        radio = ($(this).attr('data-gender'));
        $('#change_avatar').modal('show');
    });

    $("#ok_change").click(function (e) {
        e.preventDefault();
        //e.stopPropagation();
        $("#group_gender label").removeClass('active')
        $("#" + gender).val(radio);
        delete_file();
        accordion = "";

        gender = radio;
        console.log("GEN", gender);
        setTimeout(function () {
            console.log(gender);
            $("div#accordion").empty();
            $("input[type=radio][name=gender]").filter('[value=' + gender + ']').prop('checked', true);
            $("#label_" + gender).addClass("active");
            load_elements();
        }, 300);
        $('#change_avatar').modal('hide');
    });
    $(".img-check").click(function () {
        //console.log($(this));
        $(this).toggleClass("check");
    });

    $('.colorSelector').colorpicker({
        format: "hex"
    });

    $("#save").bind('click', function (e) {
        e.stopPropagation();
        e.preventDefault();
        $('#svgData').val(document.getElementById('svg_master').toXML());

        $.ajax({
            url: "php/saveImage.php/upload",
            data: $("#frmSave").serialize(),
            type: 'POST',
            success: function (rslt) {
                alert(rslt);
            },
        });
        //console.log();
    });

    userId = getUrlParameter('userid');
    if (userId == undefined)
        load_elements();
    else {
        get_elements();
        $("#userid").val(userId);
    }

    $("input[type=radio][name=gender]").filter('[value=' + gender + ']').prop('checked', true);
    $("#label_" + gender).addClass("active");
    //console.log($("input[type=radio][name=gender]").filter('[value='+gender+']').prop('checked', true));
});
Element.prototype.toXML = function () {
    return toXML(this);
}

toXML = function (element) {
    var xmlOut = "<";
    xmlOut += element.tagName;
    for (var i = 0; i < element.attributes.length; i++) {
        xmlOut += " ";
        xmlOut += element.attributes[i].name + '="';
        xmlOut += element.attributes[i].value + '"';
    }
    if (element.childNodes.length > 0) {
        xmlOut += ">";
        for (var i = 0; i < element.childNodes.length; i++) {
            if (element.childNodes[i].nodeType == 3) {
                xmlOut += element.childNodes[i].textContent;
            } else {
                xmlOut += toXML(element.childNodes[i]);
            }
        }
        xmlOut += '</' + element.tagName + '>';
    } else {
        xmlOut += '/>';
    }
    return xmlOut;
}

selec_type = function (obj, type, no) {
    $("#herramientas_" + type).css('visibility', 'visible');
    var _tipo = ($(obj).data('title'));
    var tmp = get_svg_elm(elements[_tipo][no].data.elm)
    $("#" + _tipo).html(tmp);
    var id = $(tmp).attr('id');
    $("#" + id).attr({
        'fill': '#' + elements[_tipo][no].data.fill
    });
    $("#colorSelector" + type).css({
        "color": "#" + elements[_tipo][no].data.fill
    });
    $('#colorSelector' + type).colorpicker().on('changeColor', function (e) {
        $("#" + id).attr({
            'fill': e.color.toString('hex')
        });
        $("#colorSelector" + type).css({
            "color": e.color.toString('hex')
        });
    });
}

drawList = function (title, itm, header, id_ac, con) {
    if (itm.length > 0) {
        accordion = '' +
            '<div class="card">' +
            '<div class="card-header stretch" role="tab" id="heading' + header + '">' +
            '<h6 class="mb-0">';
        if (con_draw_list == 0) {
            accordion += '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#' + id_ac +
                '" aria-expanded="true" aria-controls="' + id_ac + '">' + title +
                '</a><div class="herramientas" id="herramientas_' + con + '"><i id="colorSelector' + con +
                '" title="Cambiar color" class="colorSelector edit_color fa fa-tint text-secondary" aria-hidden="true"></i><i class="trash fa fa-trash text-danger" aria-hidden="true" name="' +
                getCleanedString(title) + '" onclick="clean(this,\'' + con + '\')" title="Limpiar"></i></div>';
        } else {
            accordion += '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#' + id_ac +
                '" aria-expanded="false" aria-controls="' + id_ac + '">' + title +
                '</a><div class="herramientas" id="herramientas_' + con + '"><i id="colorSelector' + con +
                '" title="Cambiar color" class="colorSelector edit_color fa fa-tint text-secondary" aria-hidden="true"></i><i class="trash fa fa-trash text-danger" aria-hidden="true" name="' +
                getCleanedString(title) + '" onclick="clean(this,\'' + con + '\')" title="Limpiar"></i></div>';
        }
        accordion += '</h6>' +
            '</div>';
        if (con_draw_list == 0)
            accordion += '<div id="' + id_ac + '" class="collapse show" role="tabpanel" aria-labelledby="heading' + header +
            '">';
        else
            accordion += '<div id="' + id_ac + '" class="collapse" role="tabpanel" aria-labelledby="heading' + header + '">';
        accordion += '<div class="row">';
        itm.forEach(function (itm, index) {
            if (itm.gender == gender || itm.gender == "A") {
                accordion += '<div class="col">' + '<div class=" card text-center"> ' +
                    '<div class="card-body"><div class="miniature"><label class="btn btn-default" style="cursor:pointer">';
                accordion += '<img class="img-check svg-icon img-thumbnail " src="assets/img/data_svg/thumbnails/' + itm.data.elm +
                    '"/>';
                accordion += '<input data-title="' + getCleanedString(title) + '" onclick="selec_type(this,' + con + ',' +
                    index +
                    ')" name="chk_' + con + '" type="radio" class="hidden"  autocomplete="off" value="' +
                    '' + itm.name + '">' + '</label></div>' /* + itm.name */ + '</div>' + '</div>' + '</div>';
            }
        }, this);
        accordion += '</div></div></div>';
    }
    $("#accordion").append(accordion);

    con_draw_list++;
}

load_elements = function () {
    $("#svg_master").children().empty();
    //console.log(gender);
    if (gender == "F") {
        $("#body_").html(get_svg_elm(elements.body[0].data));
    } else {
        $("#body_").html(get_svg_elm(elements.body[1].data));
    }
    let title = ['', 'Pantalón', 'Camisa', 'Zapatos', 'Cabello', 'Rostro', 'Gorro', 'Lentes', 'Cuello Accesorio'];
    Object.keys(elements).forEach(function (itm, i) {
        if (i > 0)
            drawList(title[i], elements[itm], i, 'ac_' + i, (i + 1));
    }, this);
}


delete_file = function () {
    var elm = "";
    $.ajax({
        url: "saveImage.php/remove_srv",
        data: {
            "userid": userId
        },
        dataType: "text",
        success: function (data) {
            alert(data);
            /*elm = svg.documentElement.innerHTML;
            $("#svg_master").html(elm);*/
        },
        /*error: function (xhr, ajaxOptions, thrownError) {
            if (xhr.status == 404) {load_elements();}
        }*/
    });
    return elm;
}
get_elements = function () {
    var elm = "";
    $.ajax({
        url: "uploads/" + userId,
        data: "",
        dataType: "xml",
        success: function (svg) {
            elm = svg.documentElement.innerHTML;
            $("#svg_master").html(elm);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (xhr.status == 404) {
                load_elements();
            }
        }
    });
    return elm;
}

clean = function (obj, no) {
    var _tipo = ($(obj).attr('name'));
    $("#" + _tipo).empty();
    $('input[name=chk_' + no + ']:radio').removeAttr('checked');
    $("#colorSelector" + no).css("color", "#000");
}

getCleanedString = function (cadena) {
    var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";
    for (var i = 0; i < specialChars.length; i++) {
        cadena = cadena.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
    }
    cadena = cadena.toLowerCase();
    cadena = cadena.replace(/ /g, "_");
    cadena = cadena.replace(/á/gi, "a");
    cadena = cadena.replace(/é/gi, "e");
    cadena = cadena.replace(/í/gi, "i");
    cadena = cadena.replace(/ó/gi, "o");
    cadena = cadena.replace(/ú/gi, "u");
    cadena = cadena.replace(/ñ/gi, "n");
    return cadena;
}

get_svg_elm = function (url) {
    var elm = "";
    $.ajax({
        url: "assets/img/data_svg/" + url,
        data: "",
        async: false,
        dataType: "xml",
        success: function (svg) {
            elm = svg.documentElement.innerHTML;
        }
    });
    return elm;
}

getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName, i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
